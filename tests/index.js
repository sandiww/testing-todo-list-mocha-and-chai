const chai = require("chai");
const chaihttp = require("chai-http");
const app = require("../server");

chai.use(chaihttp);
chai.should();

describe("Todos", () => {
  describe("GET /", () => {
    // Testing for endpoint GET('/todos) to get All todos with response status code 200 and return must be form object
    it("Should get all todos", (done) => {
      chai
        .request(app)
        .get("/todos")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });

    // Testing for endpoint GET(`/todos/${id}`) to get single data, that query with id response status code 200 and return must be form object
    it("Should ge a single todo", (done) => {
      const id = 1;
      chai
        .request(app)
        .get(`/todos/${id}`)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          done();
        });
    });

    // Testing for endpoint GET(/todos/${id}`) if todo not found.
    it("Should not ge a single todo", (done) => {
      const id = 10;
      chai
        .request(app)
        .get(`/todos/${id}`)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });
});
