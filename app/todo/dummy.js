const todos = [
  {
    id: 1,
    text: 'Learning Javascript',
    createdAt: new Date()
  },
  {
    id: 2,
    text: 'Learning Node JS',
    createdAt: new Date()
  },
  {
    id: 3,
    text: 'Express JS',
    createdAt: new Date()
  },
  {
    id: 4,
    text: 'Learning Mocha and Chai',
    createdAt: new Date()
  },
  {
    id: 5,
    text: 'Learning end 2 end with cypress',
    createdAt: new Date()
  }
];
module.exports = todos;